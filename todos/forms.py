from django import forms
from todos.models import TodoList

class ListForm (forms.ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]
