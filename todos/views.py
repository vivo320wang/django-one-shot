from dataclasses import field
from typing import List
from django.shortcuts import render, redirect
from todos.models import TodoItem, TodoList
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .forms import ListForm

class TodoListListView (ListView):
    model = TodoList
    template_name = "todolist/list.html"


class TodoListDetailView (DetailView):
    model = TodoList
    template_name = "todolist/detail.html"


class TodoListCreateView (CreateView):
    model = TodoList
    template_name = "todolist/create.html"
    fields = ["name"]
    success_url = reverse_lazy("todolist_detail")

    def create_list_form (request):
        if request.method == "POST":
            list_form = ListForm(request.POST)
            if list_form.is_valid():
                list = list_form.save(commit=False)
                list.save()
        return redirect("todolist_detail")
            

class TodoListUpdateView (UpdateView):
    model = TodoList
    template_name = "todolist/update.html"


class TodoListDeleteView (DeleteView):
    model = TodoList
    template_name = "todolist/delete.html"

class TodoItemCreateView (CreateView):
    model = TodoItem
    template_name = "todoitem/create.html"

    def create_item_form(request):
        if request.method == "POST":
            item_form = TodoItem(request.POST)
            if item_form.is_valid():
                item = item_form.save(commit=False)
                item.save()
        return redirect("todolist_detail")
    

class TodoItemUpdateView (UpdateView):
    model = TodoItem
    template_name = "todoitem/update.html"


# Create your views here.
